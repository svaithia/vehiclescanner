package com.planauts.vehiclescanner.resources;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.planauts.vehiclescanner.R;

public class SpinAdapter<T> extends ArrayAdapter<T> {
	private Context context;
	private List<T> values;
	
    public SpinAdapter(Context context, int textViewResourceId, List<T> makeList) {
        super(context, textViewResourceId);
        this.context = context;
        this.values = makeList;
    }
    
    public SpinAdapter(Context context, int resource, int textViewResourceId, List<T> makeList) {
        super(context, resource, textViewResourceId);
        this.context = context;
        this.values = makeList;
    }
    
    public int getCount(){
        return values.size();
     }

     public T getItem(int position){
        return values.get(position);
     }

     public long getItemId(int position){
        return position;
     }
     
//     @Override
//     public View getView(int position, View convertView, ViewGroup parent) {
//         // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
//         TextView label = new TextView(context);
//         label.setTextColor(Color.BLACK);
//         // Then you can get the current item using the values array (Users array) and the current position
//         // You can NOW reference each method you has created in your bean object (User class)
//         label.setText(values.get(position).toString());
//         
//         // And finally return your dynamic (or custom) view for each spinner item
//         return label;
//     }
//     
//     // And here is when the "chooser" is popped up
//     // Normally is the same view, but you can customize it if you want
//     @Override
//     public View getDropDownView(int position, View convertView,
//             ViewGroup parent) {
//         TextView label = new TextView(context);
//         label.setTextColor(Color.BLACK);
//         label.setText(values.get(position).toString());
//
//         return label;
//     }
     
     
     
     
     
     
     
     
     
     @Override
	  public View getDropDownView(int position, View convertView,ViewGroup parent) {
	     View row = getCustomView(position, convertView, parent);
	     TextView label=(TextView)row.findViewById(R.id.spinner_textView);
	     label.setPadding(20, 30, 20, 30);
	     label.setTextSize(24);
	     return row;
	  }
	
	 @Override
	 public View getView(int position, View convertView, ViewGroup parent) {
	     return getCustomView(position, convertView, parent);
	 }
	 
	 
	 public View getCustomView(int position, View convertView, ViewGroup parent) {
		Activity activity = (Activity) context;
		LayoutInflater inflater=activity.getLayoutInflater();
		View row=inflater.inflate(R.layout.spinner_item, parent, false);
		TextView label=(TextView)row.findViewById(R.id.spinner_textView);
		label.setText(values.get(position).toString());
		return row;
	 }
     
     
}
