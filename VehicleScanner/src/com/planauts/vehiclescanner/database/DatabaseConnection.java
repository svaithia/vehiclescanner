package com.planauts.vehiclescanner.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseConnection extends SQLiteOpenHelper {

	private static DatabaseConnection sInstance;
	private static final String DATABASE_NAME = "Vehicle_Scanner";
	private static final int DATABASE_VERSION = 1;
	 
	 
	private DatabaseConnection(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }
  
	@Override
	public void onCreate(SQLiteDatabase db) {
    	db.execSQL(DatabaseHelper.VEHICLE_TABLE_CREATE);
    	db.execSQL(DatabaseHelper.VEHICLE_INSPECTION_TABLE_CREATE);
    	db.execSQL(DatabaseHelper.TEMP_VEHICLE_INSPECTION_TABLE_CREATE);
    	db.execSQL(DatabaseHelper.USERS_TABLE_CREATE);
    	db.execSQL(DatabaseHelper.MAKES_TABLE_CREATE);
    	db.execSQL(DatabaseHelper.MODELS_TABLE_CREATE);
    	db.execSQL(DatabaseHelper.TERRITORIES_TABLE_CREATE);
    	db.execSQL(DatabaseHelper.BUILDINGS_TABLE_CREATE);
    	db.execSQL(DatabaseHelper.FLOORS_TABLE_CREATE);
    	db.execSQL(DatabaseHelper.NextServiceType_TABLE_CREATE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.VEHICLE_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.VEHICLE_INSPECTION_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.TEMP_VEHICLE_INSPECTION_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.USERS_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.MAKES_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.MODELS_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.TERRITORIES_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.BUILDINGS_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.FLOORS_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.NextServiceType_TABLE_NAME);
		onCreate(db);
	}
	
	public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }
	
	public static DatabaseConnection getInstance(Context context) {
		if (sInstance == null) {
			sInstance = new DatabaseConnection(context.getApplicationContext());
		}
		return sInstance;
	}

}
