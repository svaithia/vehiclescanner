package com.planauts.vehiclescanner.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.planauts.vehiclescanner.model.NextServiceType;

public class DBNextServiceType {
	private static String TABLE_NAME = DatabaseHelper.NextServiceType_TABLE_NAME;
	
	public static long createNextServiceType(Context context, NextServiceType nst){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put("ID", nst.getID());
		values.put("Date_Modified", nst.getDate_Modified());
		values.put("Date_Created", nst.getDate_Created());
		values.put("Name", nst.getName());
		values.put("Model_FK", nst.getModel_FK());
		
		long territory_id = db.insert(TABLE_NAME, null, values);
		return territory_id;
	}
	
	public static int updateOrInsert(Context context, NextServiceType nst){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues value = new ContentValues();
		value.put("ID", nst.getID());
		value.put("Name", nst.getName());
		value.put("Date_Created", nst.getDate_Created());
		value.put("Date_Modified", nst.getDate_Modified());
		value.put("Model_FK", nst.getModel_FK());
		
		int id = (int) db.insertWithOnConflict(TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_IGNORE);
		if(id == -1){
			id = db.update(TABLE_NAME, value, "ID = ?", new String[] { String.valueOf(nst.getID())});
		}
		
		return id;
	}
	
	public static List<NextServiceType> getAllNextServiceTypes(Context context, int model_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE Model_FK = " + model_id;

		List<NextServiceType> returnList = new ArrayList<NextServiceType>();
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int mfk = c.getInt(c.getColumnIndex("Model_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnList.add(new NextServiceType(id, dm, dc, name, mfk));
		}
		return returnList;
	}
	
	public static List<NextServiceType> getAllNextServiceTypesWithDefault(Context context, int model_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE Model_FK = " + model_id;

		List<NextServiceType> returnList = new ArrayList<NextServiceType>();
		returnList.add(new NextServiceType(0, null, null, "Select a Service Type", 0));
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int mfk = c.getInt(c.getColumnIndex("Model_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnList.add(new NextServiceType(id, dm, dc, name, mfk));
		}
		return returnList;
	}
	
	public static void syncNextServiceTypes(Context context, List<NextServiceType> nsts){
		for(NextServiceType nst : nsts){
			updateOrInsert(context, nst);
		}
	}
	
	public static NextServiceType getNextServiceType(Context context, int next_service_type_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE ID = " + next_service_type_id;

		NextServiceType returnNextServiceType = null;
		Cursor c = db.rawQuery(selectQuery, null);
		
		if(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int mfk = c.getInt(c.getColumnIndex("Model_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnNextServiceType= new NextServiceType(id, dm, dc, name, mfk);
		}
		
		return returnNextServiceType;
	}		
	
	
	
	
}
