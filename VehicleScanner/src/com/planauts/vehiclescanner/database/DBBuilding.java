package com.planauts.vehiclescanner.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.planauts.vehiclescanner.model.Building;

public class DBBuilding {
	private static String TABLE_NAME = DatabaseHelper.BUILDINGS_TABLE_NAME;
	
	public static long createBuildings(Context context, Building building){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put("ID", building.getID());
		values.put("Date_Modified", building.getDate_Modified());
		values.put("Date_Created", building.getDate_Created());
		values.put("Name", building.getName());
		values.put("Territory_FK", building.getTerritory_FK());
		
		long territory_id = db.insert(TABLE_NAME, null, values);
		return territory_id;
	}
	
	public static int updateOrInsert(Context context, Building building){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues value = new ContentValues();
		value.put("ID", building.getID());
		value.put("Name", building.getName());
		value.put("Date_Created", building.getDate_Created());
		value.put("Date_Modified", building.getDate_Modified());
		value.put("Territory_FK", building.getTerritory_FK());
		
		int id = (int) db.insertWithOnConflict(TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_IGNORE);
		if(id == -1){
			id = db.update(TABLE_NAME, value, "ID = ?", new String[] { String.valueOf(building.getID())});
		}
		
		return id;
	}
	
	public static List<Building> getAllBuildings(Context context, int territory_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE Territory_FK = " + territory_id;

		List<Building> returnList = new ArrayList<Building>();
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){ 
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int territory_fk = c.getInt(c.getColumnIndex("Territory_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnList.add(new Building(id, dm, dc, name, territory_fk));
		}
		return returnList;
	}
	
	public static List<Building> getAllBuildingsWithDefault(Context context, int territory_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE Territory_FK = " + territory_id;

		List<Building> returnList = new ArrayList<Building>();
		returnList.add(new Building(0, null, null, "Select a Building", 0));
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){ 
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int territory_fk = c.getInt(c.getColumnIndex("Territory_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnList.add(new Building(id, dm, dc, name, territory_fk));
		}
		return returnList;
	}
	
	public static void syncTerritory(Context context, List<Building> buildings){
		for(Building building : buildings){
			updateOrInsert(context, building);
		}
	}
	
	public static Building getBuilding(Context context, int floor_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE ID = " + floor_id;

		Building returnBuilding = null;
		Cursor c = db.rawQuery(selectQuery, null);
		
		if(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int tfk = c.getInt(c.getColumnIndex("Territory_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnBuilding = new Building(id, dm, dc, name, tfk);
		}
		
		return returnBuilding;
	}		
	
	
}
