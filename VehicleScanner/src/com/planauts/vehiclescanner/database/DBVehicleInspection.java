package com.planauts.vehiclescanner.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.planauts.vehiclescanner.model.VehicleInspection;

public class DBVehicleInspection {
	public static long createVehicleInspection(Context context, VehicleInspection vi){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put("vehicle_barcode", vi.getVehicle_barcode());
		values.put("date_synced", vi.getDate_synced());
		values.put("notes", vi.getNotes());
		values.put("status", vi.getStatus());
		values.put("date_modified", vi.getDate_modified());
		values.put("id", vi.getId());
		values.put("user_id", vi.getUser_Id());
		
		long vehicle_inspection = db.insert(DatabaseHelper.VEHICLE_INSPECTION_TABLE_NAME, null, values);
		return vehicle_inspection;
	}
	
	public static VehicleInspection getRecentVehicleInspection(Context context, int vehicle_id, boolean tempTable){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		
		String table = tempTable ? DatabaseHelper.TEMP_VEHICLE_INSPECTION_TABLE_NAME : DatabaseHelper.VEHICLE_INSPECTION_TABLE_NAME;
		
		Cursor c = db.query(table, null, "vehicle_barcode=?", new String[]{String.valueOf(vehicle_id)}, null, null, "date_modified DESC", "1");
		
		VehicleInspection vi = new VehicleInspection();
		
		if(c.moveToFirst()){
			vi.setId(c.getInt(c.getColumnIndex("id")));
			vi.setDate_synced(c.getString(c.getColumnIndex("date_synced")));
			vi.setVehicle_barcode(c.getInt(c.getColumnIndex("vehicle_barcode")));
			vi.setNotes(c.getString(c.getColumnIndex("notes")));
			vi.setStatus(c.getInt(c.getColumnIndex("status")));
			vi.setDate_modified(c.getString(c.getColumnIndex("date_modified")));
			vi.setUser_Id(c.getInt(c.getColumnIndex("user_id")));
			c.close();
		} else{
			vi.setVehicle_barcode(vehicle_id);;
		}
		return vi;
	}

	public static long createTempVehicleInspection(Context context, VehicleInspection vi){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put("vehicle_barcode", vi.getVehicle_barcode());
		values.put("date_synced", vi.getDate_synced());
		values.put("notes", vi.getNotes());
		values.put("status", vi.getStatus());
		values.put("date_modified", vi.getDate_modified());
		values.put("user_id", vi.getUser_Id());
		long vehicle_inspection = db.insert(DatabaseHelper.TEMP_VEHICLE_INSPECTION_TABLE_NAME, null, values);
		return vehicle_inspection;
	}
	
	
	public static List<VehicleInspection> getPushableVehicleInspections(Context context){
		List<VehicleInspection> vehicleInspectionList = new ArrayList<VehicleInspection>();
		
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + DatabaseHelper.TEMP_VEHICLE_INSPECTION_TABLE_NAME + " WHERE " + "1" ;
		Cursor c = db.rawQuery(selectQuery, null);
		
		while(c.moveToNext()){
			VehicleInspection vi = new VehicleInspection();
			vi.setDate_modified(c.getString(c.getColumnIndex("date_modified")));
			vi.setId(c.getInt(c.getColumnIndex("id")));
			vi.setNotes(c.getString(c.getColumnIndex("notes")));
			vi.setStatus(c.getInt(c.getColumnIndex("status")));
			vi.setVehicle_barcode(c.getInt(c.getColumnIndex("vehicle_barcode")));
			vi.setUser_Id(c.getInt(c.getColumnIndex("user_id")));
			vehicleInspectionList.add(vi);
		}
		return vehicleInspectionList;
	}
}
