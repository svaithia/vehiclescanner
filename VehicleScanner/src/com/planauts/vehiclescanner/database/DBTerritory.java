package com.planauts.vehiclescanner.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.planauts.vehiclescanner.model.Building;
import com.planauts.vehiclescanner.model.Territory;

public class DBTerritory {
	private static String TABLE_NAME = DatabaseHelper.TERRITORIES_TABLE_NAME;
	
	public static long createMake(Context context, Territory territory){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put("ID", territory.getID());
		values.put("Date_Modified", territory.getDate_Modified());
		values.put("Date_Created", territory.getDate_Created());
		values.put("Name", territory.getName());
		
		long territory_id = db.insert(TABLE_NAME, null, values);
		return territory_id;
	}
	
	public static int updateOrInsert(Context context, Territory make){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues value = new ContentValues();
		value.put("ID", make.getID());
		value.put("Name", make.getName());
		value.put("Date_Created", make.getDate_Created());
		value.put("Date_Modified", make.getDate_Modified());
		
		int id = (int) db.insertWithOnConflict(TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_IGNORE);
		if(id == -1){
			id = db.update(TABLE_NAME, value, "ID = ?", new String[] { String.valueOf(make.getID())});
		}
		
		return id;
	}
	
	public static List<Territory> getAllTerritories(Context context){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME;

		List<Territory> returnList = new ArrayList<Territory>();
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){
			
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnList.add(new Territory(id, dm, dc, name));
		}
		return returnList;
	}
	
	public static List<Territory> getAllTerritoriesWithDefault(Context context){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME;

		List<Territory> returnList = new ArrayList<Territory>();
		returnList.add(new Territory(0, null, null, "Select a Territory"));
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){
			
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnList.add(new Territory(id, dm, dc, name));
		}
		return returnList;
	}
	
	public static void syncTerritory(Context context, List<Territory> territories){
		for(Territory territory : territories){
			updateOrInsert(context, territory);
		}
	}
	
	public static Territory getTerritory(Context context, int territory_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE ID = " + territory_id;

		Territory returnTerritory = null;
		Cursor c = db.rawQuery(selectQuery, null);
		
		if(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnTerritory = new Territory(id, dm, dc, name);
		}
		
		return returnTerritory;
	}		
	
}
