package com.planauts.vehiclescanner.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.planauts.vehiclescanner.model.Make;
import com.planauts.vehiclescanner.model.Model;

public class DBModel {
	private static String TABLE_NAME = DatabaseHelper.MODELS_TABLE_NAME;
	
	public static long createModels(Context context, Model model){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put("ID", model.getID());
		values.put("Date_Modified", model.getDate_Modified());
		values.put("Date_Created", model.getDate_Created());
		values.put("Name", model.getName());
		values.put("Make_FK", model.getMake_FK());
		values.put("Sort", model.getSort());
		
		long territory_id = db.insert(TABLE_NAME, null, values);
		return territory_id;
	}
	
	public static int updateOrInsert(Context context, Model model){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues value = new ContentValues();
		value.put("ID", model.getID());
		value.put("Name", model.getName());
		value.put("Date_Created", model.getDate_Created());
		value.put("Date_Modified", model.getDate_Modified());
		value.put("Make_FK", model.getMake_FK());
		value.put("Sort", model.getSort());
		
		int id = (int) db.insertWithOnConflict(TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_IGNORE);
		if(id == -1){
			id = db.update(TABLE_NAME, value, "ID = ?", new String[] { String.valueOf(model.getID())});
		}
		
		return id;
	}
	
	public static List<Model> getAllModel(Context context, int make_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE Make_FK = " + make_id;

		List<Model> returnList = new ArrayList<Model>();
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int make_fk = c.getInt(c.getColumnIndex("Make_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			int sort = c.getInt(c.getColumnIndex("Sort"));
			
			returnList.add(new Model(id, dm, dc, name, make_fk, sort));
		}
		return returnList;
	}
	
	public static List<Model> getAllModelWithDefault(Context context, int make_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE Make_FK = " + make_id + " ORDER BY Sort ";

		List<Model> returnList = new ArrayList<Model>();
		returnList.add(new Model(0, null, null, "Select a model", 0, 0));
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int make_fk = c.getInt(c.getColumnIndex("Make_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			int sort = c.getInt(c.getColumnIndex("Sort"));
			
			returnList.add(new Model(id, dm, dc, name, make_fk, sort));
		}
		return returnList;
	}
	
	public static void syncTerritory(Context context, List<Model> models){
		for(Model model : models){
			updateOrInsert(context, model);
		}
	}
	
	public static Model getModel(Context context, int model_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE ID = " + model_id;

		Model returnModel = null;
		Cursor c = db.rawQuery(selectQuery, null);
		
		if(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int make_fk = c.getInt(c.getColumnIndex("Make_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			int sort = c.getInt(c.getColumnIndex("Sort"));
			
			returnModel = new Model(id, dm, dc, name, make_fk, sort);
		}
		
		return returnModel;
	}		
	
	
}
