package com.planauts.vehiclescanner.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.planauts.vehiclescanner.model.Floor;

public class DBFloor {
	private static String TABLE_NAME = DatabaseHelper.FLOORS_TABLE_NAME;
	
	public static long createBuildings(Context context, Floor floor){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put("ID", floor.getID());
		values.put("Date_Modified", floor.getDate_Modified());
		values.put("Date_Created", floor.getDate_Created());
		values.put("Name", floor.getName());
		values.put("Building_FK", floor.getBuilding_FK());
		
		long territory_id = db.insert(TABLE_NAME, null, values);
		return territory_id;
	}
	
	public static int updateOrInsert(Context context, Floor floor){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues value = new ContentValues();
		value.put("ID", floor.getID());
		value.put("Name", floor.getName());
		value.put("Date_Created", floor.getDate_Created());
		value.put("Date_Modified", floor.getDate_Modified());
		value.put("Building_FK", floor.getBuilding_FK());
		
		int id = (int) db.insertWithOnConflict(TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_IGNORE);
		if(id == -1){
			id = db.update(TABLE_NAME, value, "ID = ?", new String[] { String.valueOf(floor.getID())});
		}
		
		return id;
	}
	
	public static List<Floor> getAllFloors(Context context, int building_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE Building_FK = " + building_id;

		List<Floor> returnList = new ArrayList<Floor>();
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int bfk = c.getInt(c.getColumnIndex("Building_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnList.add(new Floor(id, dm, dc, name, bfk));
		}
		return returnList;
	}
	
	public static List<Floor> getAllFloorsWithDefault(Context context, int building_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE Building_FK = " + building_id;

		List<Floor> returnList = new ArrayList<Floor>();
		returnList.add(new Floor(0, null, null, "Select a Floor", 0));
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int bfk = c.getInt(c.getColumnIndex("Building_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnList.add(new Floor(id, dm, dc, name, bfk));
		}
		return returnList;
	}
	
	public static void syncFloors(Context context, List<Floor> floors){
		for(Floor floor : floors){
			updateOrInsert(context, floor);
		}
	}
	
	public static Floor getFloor(Context context, int floor_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE ID = " + floor_id;

		Floor returnFloor = null;
		Cursor c = db.rawQuery(selectQuery, null);
		
		if(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int bfk = c.getInt(c.getColumnIndex("Building_FK"));
			int id = c.getInt(c.getColumnIndex("ID"));
			
			returnFloor= new Floor(id, dm, dc, name, bfk);
		}
		
		return returnFloor;
	}		
	
}
