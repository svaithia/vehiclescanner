package com.planauts.vehiclescanner.database;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DatabaseHelper {
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	
	public static final String VEHICLE_TABLE_NAME = "vehicle";
	public static final String VEHICLE_TABLE_CREATE = "CREATE TABLE " + VEHICLE_TABLE_NAME + "( "
					+ "date_synced DATETIME DEFAULT NULL,"
					+ "date_modified DATETIME DEFAULT NULL,"
					+ "barcode INT PRIMARY KEY NOT NULL,"
					+ "Model_FK INT, "
					+ "Floor_FK INT, "
					+ "Next_Service_Type_FK INT, "
					+ "Next_Service_Date DATE DEFAULT NULL, "
					+ "Year INT, "
					+ "help_check_list CHAR(400), "
					+ "description CHAR(200)"
					+ ");";
	
	public static final String VEHICLE_INSPECTION_TABLE_NAME = "vehicle_inspection";
	public static final String VEHICLE_INSPECTION_TABLE_CREATE = "CREATE TABLE " + VEHICLE_INSPECTION_TABLE_NAME + "( "
			+ "id INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "date_synced DATETIME DEFAULT NULL, "
			+ "date_modified DATETIME DEFAULT NULL, "
			+ "vehicle_barcode INT NOT NULL, "
			+ "notes  CHAR(200), "
			+ "user_id INT NOT NULL, " 
			+ "status INT NOT NULL "
			+ ");";
	
	public static final String TEMP_VEHICLE_INSPECTION_TABLE_NAME = "temp_vehicle_inspection";
	public static final String TEMP_VEHICLE_INSPECTION_TABLE_CREATE = ""
			+ "CREATE TABLE " + TEMP_VEHICLE_INSPECTION_TABLE_NAME  + "( "
			+ "id INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "date_synced DATETIME DEFAULT NULL, "
			+ "date_modified DATETIME DEFAULT NULL, "
			+ "vehicle_barcode INT NOT NULL, "
			+ "notes  CHAR(200), "
			+ "user_id INT NOT NULL, " 
			+ "status INT NOT NULL "
			+ ");";
	
	public static final String USERS_TABLE_NAME = "users";
	public static final String USERS_TABLE_CREATE = ""
			+ "CREATE TABLE " + USERS_TABLE_NAME + " ( "
			+ "id INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "email CHAR(100),"
			+ "password CHAR(200), "
			+ "server_id INTEGER NOT NULL, "
			+ "Permission_Level INTEGER DEFAULT 0"
			+ ");";
	
	public static final String MAKES_TABLE_NAME = "Makes";
	public static final String MAKES_TABLE_CREATE = ""
			+ "CREATE TABLE " + MAKES_TABLE_NAME + " ( "
			+ "ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "Date_Modified DATETIME DEFAULT NULL,"
			+ "Date_Created DATETIME DEFAULT NULL, "
			+ "Name CHAR(200) NOT NULL, "
			+ "Sort INT DEFAULT 0"
			+ ");";


	public static final String MODELS_TABLE_NAME = "Models";
	public static final String MODELS_TABLE_CREATE = ""
			+ "CREATE TABLE " + MODELS_TABLE_NAME + " ( "
			+ "ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "Date_Modified DATETIME DEFAULT NULL,"
			+ "Date_Created DATETIME DEFAULT NULL, "
			+ "Make_FK INTEGER NOT NULL, "
			+ "Name CHAR(200) NOT NULL, "
			+ "Sort INT DEFAULT 0 "
			+ ");";

	public static final String TERRITORIES_TABLE_NAME = "Territories";
	public static final String TERRITORIES_TABLE_CREATE = ""
			+ "CREATE TABLE " + TERRITORIES_TABLE_NAME + " ( "
			+ "ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "Date_Modified DATETIME DEFAULT NULL,"
			+ "Date_Created DATETIME DEFAULT NULL, "
			+ "Name CHAR(200) NOT NULL "
			+ ");";


	public static final String BUILDINGS_TABLE_NAME = "Buildings";
	public static final String BUILDINGS_TABLE_CREATE = ""
			+ "CREATE TABLE " + BUILDINGS_TABLE_NAME + " ( "
			+ "ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "Date_Modified DATETIME DEFAULT NULL,"
			+ "Date_Created DATETIME DEFAULT NULL, "
			+ "Territory_FK INTEGER NOT NULL, "
			+ "Name CHAR(200) NOT NULL "
			+ ");";
	
	public static final String NextServiceType_TABLE_NAME = "Next_Service_Type";
	public static final String NextServiceType_TABLE_CREATE = ""
			+ "CREATE TABLE " + NextServiceType_TABLE_NAME + " ( "
			+ "ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "Date_Modified DATETIME DEFAULT NULL,"
			+ "Date_Created DATETIME DEFAULT NULL, "
			+ "Model_FK INTEGER NOT NULL, "
			+ "Name CHAR(200) NOT NULL "
			+ ");";
	
	public static final String FLOORS_TABLE_NAME = "Floors";
	public static final String FLOORS_TABLE_CREATE = ""
			+ "CREATE TABLE " + FLOORS_TABLE_NAME + " ( "
			+ "ID INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "Date_Modified DATETIME DEFAULT NULL,"
			+ "Date_Created DATETIME DEFAULT NULL, "
			+ "Building_FK INTEGER NOT NULL, "
			+ "Name CHAR(200) NOT NULL "
			+ ");";
	
			
	public static void cleanUpSync(Context context){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		db.delete(TEMP_VEHICLE_INSPECTION_TABLE_NAME, null, null);
	}
	
	public static void deleteAll(Context context){
		Log.i("deleteAll", "start");
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		db.delete(VEHICLE_TABLE_NAME, null, null);
		db.delete(TEMP_VEHICLE_INSPECTION_TABLE_NAME, null, null);
		db.delete(VEHICLE_INSPECTION_TABLE_NAME, null, null);
		db.delete(USERS_TABLE_NAME, null, null);
		db.delete(MAKES_TABLE_NAME, null, null);
		db.delete(MODELS_TABLE_NAME, null, null);
		db.delete(TERRITORIES_TABLE_NAME, null, null);
		db.delete(NextServiceType_TABLE_NAME, null, null);
		db.delete(BUILDINGS_TABLE_NAME, null, null);
		db.delete(FLOORS_TABLE_NAME, null, null);
		Log.i("deleteAll", "end");
	}
}
