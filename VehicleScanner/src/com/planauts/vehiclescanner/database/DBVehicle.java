package com.planauts.vehiclescanner.database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.planauts.vehiclescanner.model.Vehicle;

public class DBVehicle {

	public static long createVehicle(Context context, Vehicle v){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put("barcode", v.getBarcode());
		values.put("Model_FK", v.getModel_FK());
		values.put("Floor_FK", v.getFloor_FK());
		values.put("Next_Service_Type_FK", v.getNext_Service_Type_FK());
		values.put("description", v.getDescription());
		values.put("date_synced", v.getDate_synced());
		values.put("date_modified", v.getDate_modified());
		values.put("help_check_list", v.getHelp_check_list());
		values.put("Year", v.getYear());
		values.put("Next_Service_Date", DatabaseHelper.DATE_FORMAT.format(v.getNext_Service_Date().getTime()));
		
		long vehicle_id = db.insert(DatabaseHelper.VEHICLE_TABLE_NAME, null, values);
		return vehicle_id;
	}
	
	public static Vehicle getVehicle(Context context, long vehicle_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		
		String selectQuery = "SELECT * FROM " + DatabaseHelper.VEHICLE_TABLE_NAME + " WHERE "
				+ "barcode = " + vehicle_id;
		
		Cursor c = db.rawQuery(selectQuery, null);
		Vehicle v = new Vehicle();
		if(c.moveToFirst()){
			v.setBarcode(c.getInt(c.getColumnIndex("barcode")));
			v.setModel_FK(c.getInt(c.getColumnIndex("Model_FK")));
			v.setFloor_FK(c.getInt(c.getColumnIndex("Floor_FK")));
			v.setNext_Service_Type_FK(c.getInt(c.getColumnIndex("Next_Service_Type_FK")));
			v.setDescription(c.getString(c.getColumnIndex("description")));
			v.setDate_synced(c.getString(c.getColumnIndex("date_synced")));
			v.setDate_modified(c.getString(c.getColumnIndex("date_modified")));
			v.setHelp_check_list(c.getString(c.getColumnIndex("help_check_list")));
			v.setNext_Service_Date(c.getString(c.getColumnIndex("Next_Service_Date")));
			v.setYear(c.getInt(c.getColumnIndex("Year")));
			c.close();
		} else {
			v.setBarcode((int)vehicle_id);
		}
		return v;
	}
	
	public static int updateVehicle(Context context, Vehicle v){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		
		ContentValues values = new ContentValues();
		values.put("barcode", v.getBarcode());
		values.put("Model_FK", v.getModel_FK());
		values.put("Floor_FK", v.getFloor_FK());
		values.put("description", v.getDescription());
		values.put("date_synced", v.getDate_synced());
		values.put("date_modified", v.getDate_modified());
		values.put("help_check_list", v.getHelp_check_list());
		values.put("Next_Service_Type_FK", v.getNext_Service_Type_FK());
		values.put("Next_Service_Date", DatabaseHelper.DATE_FORMAT.format(v.getNext_Service_Date().getTime()));
		values.put("Year", v.getYear());
		
		int returnValue =  db.update(DatabaseHelper.VEHICLE_TABLE_NAME, values, "barcode = ?", new String[] { String.valueOf(v.getBarcode())});
		return returnValue;
	}
	
	public static List<Vehicle> getPushableVehicles(Context context){
		List<Vehicle> vehicleList = new ArrayList<Vehicle>();
		
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + DatabaseHelper.VEHICLE_TABLE_NAME + " WHERE " + "date_synced is null" ;
		Cursor c = db.rawQuery(selectQuery, null);
		
		while(c.moveToNext()){
			Vehicle v = new Vehicle(
				c.getInt(c.getColumnIndex("barcode")),
				c.getString(c.getColumnIndex("description")),
				c.getInt(c.getColumnIndex("Model_FK")),
				c.getInt(c.getColumnIndex("Year")),
				c.getInt(c.getColumnIndex("Floor_FK")),
				c.getString(c.getColumnIndex("Next_Service_Date")),
				c.getString(c.getColumnIndex("help_check_list")),
				c.getString(c.getColumnIndex("date_modified")),
				null,
				c.getInt(c.getColumnIndex("Next_Service_Type_FK"))
			);
			vehicleList.add(v);
		}
		return vehicleList;
	}
	
	public static void syncVehicles(Context context, List<Vehicle> vehicle){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		// Delete sync vehicles
		StringBuilder whereClause = new StringBuilder();
		List<String> whereArgs = new ArrayList<String>();
		
		for(int i=0; i<vehicle.size(); i++){
			if(i!=0){
				whereClause.append(" OR ");
			}
			whereClause.append(" barcode = ? ");
			whereArgs.add(String.valueOf(vehicle.get(i).getBarcode()));
		}
		
		db.delete(DatabaseHelper.VEHICLE_TABLE_NAME, whereClause.toString(), whereArgs.toArray(new String[whereArgs.size()]));
		
		// Insert sync vehicles
		for(int i=0; i<vehicle.size(); i++){
			DBVehicle.createVehicle(context, vehicle.get(i));
		}
	}
	public static void createOrUpdateVehicle(Context context, Vehicle v){
		Vehicle existing = DBVehicle.getVehicle(context, v.getBarcode());
		boolean update = false;
		boolean create = false;
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
				if(existing.getDate_modified() == null){
					create = true;
				} else {
					Date existingDate = sdf.parse(existing.getDate_modified());
					Date now = sdf.parse(v.getDate_modified());
					if(now.after(existingDate)){
						update = true;
					}
				}
			} catch (ParseException e) {
				create = true;
			}
		
			if(create){
				DBVehicle.createVehicle(context, v);
			} else if(update){
				DBVehicle.updateVehicle(context, v);
			}
	}
	
	public static String getVehicleHelpNotes(Context context, int barcode){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + DatabaseHelper.VEHICLE_TABLE_NAME + " WHERE "
				+ "barcode = " + String.valueOf(barcode);

		String returnValue = "Help Docs Not Found!";
		
		Cursor c = db.rawQuery(selectQuery, null);
		if(c.moveToFirst()){
			returnValue = c.getString(c.getColumnIndex("help_check_list"));
			c.close();
		}
		return returnValue;
	}
	
//	public static String getMake_Model(Context context, int model_id){
//		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
//		SQLiteDatabase db = dbConnection.getReadableDatabase();
//		
//		String selectQuery = "SELECT Model.Name, Make.Name "
//				+ " FROM Model "
//				+ " JOIN Make ON Model.ID = Make.ID "
//				+ " WHERE Model.ID = " + model_id;
//		Cursor c = db.rawQuery(selectQuery, null);
//		String returnValue = "";
//		if(c.moveToFirst()){
//			returnValue = c.getString(c.getColumnIndex("Model.Name")) + " - " + c.getString(c.getColumnIndex("Model.Name"));
//			c.close();
//		}
//		return returnValue;
//	}
	
}
