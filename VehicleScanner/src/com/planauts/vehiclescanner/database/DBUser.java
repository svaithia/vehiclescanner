package com.planauts.vehiclescanner.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.planauts.vehiclescanner.service.tools.HashingService;

public class DBUser {
	
	public static void createUser(Context context, String email, String literal_password, int server_id, int permission_level){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues user = new ContentValues();
		user.put("email", email);
		user.put("password", HashingService.md5(literal_password));
		user.put("server_id", server_id);
		user.put("Permission_Level", permission_level);
		db.insert(DatabaseHelper.USERS_TABLE_NAME, null, user);
	}

	public static Integer[] userMatches(Context context, String email, String literal_password){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + DatabaseHelper.USERS_TABLE_NAME + " WHERE "
				+ "email = '" + email + "' AND password = '" + HashingService.md5(literal_password) + "'";
	
		Cursor c = db.rawQuery(selectQuery, null);
		Integer[] returnValue = {0,0};
		if(c.moveToFirst()){
			returnValue[0] = c.getInt(c.getColumnIndex("server_id"));
			returnValue[1] = c.getInt(c.getColumnIndex("Permission_Level"));
			c.close();
		}
		return returnValue;
	}
}
