package com.planauts.vehiclescanner.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.planauts.vehiclescanner.model.Make;
import com.planauts.vehiclescanner.model.Model;

public class DBMake {
	private static String TABLE_NAME = DatabaseHelper.MAKES_TABLE_NAME;
	
	public static long createMake(Context context, Make make){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put("ID", make.getID());
		values.put("Date_Modified", make.getDate_Modified());
		values.put("Date_Created", make.getDate_Created());
		values.put("Name", make.getName());
		values.put("Sort", make.getSort());
		
		long vehicle_id = db.insert(TABLE_NAME, null, values);
		return vehicle_id;
	}
	
	public static int updateOrInsert(Context context, Make make){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		
		ContentValues value = new ContentValues();
		value.put("ID", make.getID());
		value.put("Name", make.getName());
		value.put("Date_Created", make.getDate_Created());
		value.put("Date_Modified", make.getDate_Modified());
		value.put("Sort", make.getSort());
		
		int id = (int) db.insertWithOnConflict(TABLE_NAME, null, value, SQLiteDatabase.CONFLICT_IGNORE);
		if(id == -1){
			id = db.update(TABLE_NAME, value, "ID = ?", new String[] { String.valueOf(make.getID())});
		}
		
		return id;
	}
	
	public static List<Make> getAllMakes(Context context){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " ORDER BY Sort";

		List<Make> returnList = new ArrayList<Make>();
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){
			
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int id = c.getInt(c.getColumnIndex("ID"));
			int sort = c.getInt(c.getColumnIndex("Sort"));
			
			returnList.add(new Make(id, dm, dc, name, sort));
		}
		return returnList;
	}
	
	public static List<Make> getAllMakesWithDefault(Context context){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " ORDER BY Sort ";

		List<Make> returnList = new ArrayList<Make>();
		returnList.add(new Make(0, "", "", "Select a Make", 0));
		Cursor c = db.rawQuery(selectQuery, null);
		while(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int id = c.getInt(c.getColumnIndex("ID"));
			int sort = c.getInt(c.getColumnIndex("Sort"));
			
			returnList.add(new Make(id, dm, dc, name, sort));
		}
		return returnList;
	}
	
//	public static String[] getAllMakes(Context context){
//		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
//		SQLiteDatabase db = dbConnection.getReadableDatabase();
//		String selectQuery = "SELECT Name, ID FROM " + TABLE_NAME;
//
//		List<String> returnList = new ArrayList<String>();
//		Cursor c = db.rawQuery(selectQuery, null);
//		while(c.moveToNext()){
//			String name = c.getString(c.getColumnIndex("Name"));
//			returnList.add(name);
//		}
//		return returnList.toArray(new String[returnList.size()]);
//	}
//	
	public static void syncMakes(Context context, List<Make> makes){
		for(Make make : makes){
			updateOrInsert(context, make);
		}
	}
	
	public static Make getMake(Context context, int make_id){
		DatabaseConnection dbConnection = DatabaseConnection.getInstance(context);
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE ID = " + make_id;

		Make returnMake = null;
		Cursor c = db.rawQuery(selectQuery, null);
		
		if(c.moveToNext()){
			String name = c.getString(c.getColumnIndex("Name"));
			String dm = c.getString(c.getColumnIndex("Date_Modified"));
			String dc = c.getString(c.getColumnIndex("Date_Created"));
			int id = c.getInt(c.getColumnIndex("ID"));
			int sort = c.getInt(c.getColumnIndex("Sort"));
			
			returnMake = new Make(id, dm, dc, name, sort);
		}
		
		return returnMake;
	}
	
}
