package com.planauts.vehiclescanner.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;

import com.planauts.vehiclescanner.database.DBBuilding;
import com.planauts.vehiclescanner.database.DBFloor;
import com.planauts.vehiclescanner.database.DBMake;
import com.planauts.vehiclescanner.database.DBModel;
import com.planauts.vehiclescanner.database.DBNextServiceType;
import com.planauts.vehiclescanner.database.DBTerritory;
import com.planauts.vehiclescanner.database.DBVehicle;
import com.planauts.vehiclescanner.database.DBVehicleInspection;
import com.planauts.vehiclescanner.database.DatabaseHelper;
import com.planauts.vehiclescanner.model.Building;
import com.planauts.vehiclescanner.model.Floor;
import com.planauts.vehiclescanner.model.Make;
import com.planauts.vehiclescanner.model.Model;
import com.planauts.vehiclescanner.model.NextServiceType;
import com.planauts.vehiclescanner.model.Territory;
import com.planauts.vehiclescanner.model.Vehicle;
import com.planauts.vehiclescanner.model.VehicleInspection;
import com.planauts.vehiclescanner.resources.Contents;
import com.planauts.vehiclescanner.service.tools.UserService;

public class ResetService extends AsyncTask<String, Void, String> {
	private List<NameValuePair> nameValuePairs;
	private String serial = "unknown";
	private Context context;
	private TextView textview;
	
	public ResetService(Context context, TextView tv){
		serial = UserService.getSerial(context);
		nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("serial", serial));
		this.context = context;
		textview = tv;
	}
	
	@Override
	protected String doInBackground(String... params) {
		String url = Contents.RESET_DATA_URL;
		String response = "";
        response = SyncHelper.makeRequest(url, nameValuePairs);
        return response;
	}

	@Override
	protected void onPostExecute(String response){
		Log.i(this.getClass().getName(), response);
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(response);
			String sync_time = jsonObj.getString("date");
			String success = jsonObj.getString("success");
			String msg = jsonObj.getString("msg");
			DatabaseHelper.deleteAll(context);
			
			if(success.equals("true")){
				JSONArray vehicle = jsonObj.getJSONArray("vehicles");
				JSONArray vehicle_inspection = jsonObj.getJSONArray("vehicle_inspections");
				JSONArray makes = jsonObj.getJSONArray("makes");
				JSONArray models = jsonObj.getJSONArray("models");
				JSONArray territories = jsonObj.getJSONArray("territories");
				JSONArray buildings = jsonObj.getJSONArray("buildings");
				JSONArray floors = jsonObj.getJSONArray("floors");
				JSONArray next_service_types = jsonObj.getJSONArray("next_service_types");
				
				for (int i=0; i < vehicle.length(); i++) {
				    JSONObject obj = vehicle.getJSONObject(i);
				    Vehicle v = new Vehicle();
				    v.readJSON(obj);
				    DBVehicle.createVehicle(context, v);;
				}
				
				for (int i=0; i < next_service_types.length(); i++) {
				    JSONObject obj = next_service_types.getJSONObject(i);
				    NextServiceType v = new NextServiceType();
				    v.readJSON(obj);
				    DBNextServiceType.updateOrInsert(context, v);
				}
				
				for (int i=0; i < vehicle_inspection.length(); i++) {
				    JSONObject obj = vehicle_inspection.getJSONObject(i);
				    VehicleInspection vi = new VehicleInspection();
				    vi.readJSON(obj);
				    DBVehicleInspection.createVehicleInspection(context, vi);
				}
				
				for(int i=0; i<makes.length(); i++){
					JSONObject obj = makes.getJSONObject(i);
					Make make = new Make();
					make.readJSON(obj);
					DBMake.updateOrInsert(context, make);
				}
				
				for(int i=0; i<models.length(); i++){
					JSONObject obj = models.getJSONObject(i);
					Model model = new Model();
					model.readJSON(obj);
					DBModel.updateOrInsert(context, model);
				}
				
				for(int i=0; i<territories.length(); i++){
					JSONObject obj = territories.getJSONObject(i);
					Territory territory = new Territory();
					territory.readJSON(obj);
					DBTerritory.updateOrInsert(context, territory);
				}
				
				for(int i=0; i<buildings.length(); i++){
					JSONObject obj = buildings.getJSONObject(i);
					Building building = new Building();
					building.readJSON(obj);
					DBBuilding.updateOrInsert(context, building);
				}
				
				for(int i=0; i<floors.length(); i++){
					JSONObject obj = floors.getJSONObject(i);
					Floor floor = new Floor();
					floor.readJSON(obj);
					DBFloor.updateOrInsert(context, floor);
				}
				
				
				SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
				SharedPreferences.Editor editor = app_preferences.edit();
				editor.putString("last_synced", sync_time);
				editor.commit();
				
				AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
	        	alertbox.setMessage("Reset database at server time: " + sync_time);
	        	alertbox.create();
	        	alertbox.show();
				
				if(textview != null){
					textview.setText(sync_time);
					textview.setTextColor(Color.GREEN);
				}
			} else {
				AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
	        	alertbox.setMessage("Reset error: " + msg);
	        	alertbox.create();
	        	alertbox.show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
}
