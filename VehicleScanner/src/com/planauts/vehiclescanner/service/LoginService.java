package com.planauts.vehiclescanner.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.planauts.vehiclescanner.activity.ScanInputActivity;
import com.planauts.vehiclescanner.database.DBUser;
import com.planauts.vehiclescanner.resources.Contents;

public class LoginService extends AsyncTask<String, String, Integer[]> {
	private final Context context;
	private ProgressDialog mDialog;
	private String email;
	private String password;
	
	public LoginService(Context context){
		this.context = context;
	}
	
	@Override
    protected void onPreExecute() {
        super.onPreExecute();

        mDialog = new ProgressDialog(context);
        mDialog.setMessage("Please wait... Searching for user ...");
        mDialog.show();
    }
	
	@Override
	protected Integer[] doInBackground(String... params) {
		Integer responseInteger[] = {0,0}; // [ user_id , Permission_Level] 
		email = params[0];
	    password = params[1];
	    Integer[] matchFoundInDatabase = DBUser.userMatches(context, email, password);
		
		if(matchFoundInDatabase[0] != 0 || matchFoundInDatabase[1] != 0){
			responseInteger[0] = matchFoundInDatabase[0];
			responseInteger[1] = matchFoundInDatabase[1];
		} else{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(Contents.LOGIN_URL);
			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("email", email));
				nameValuePairs.add(new BasicNameValuePair("password", password));
			    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			    // Execute HTTP Post Request
			    HttpResponse response = httpclient.execute(httppost);
			    HttpEntity entity = response.getEntity();
			    InputStream is = entity.getContent();
			    String responseString = SyncHelper.convertStreamToString(is);
			    
			    // Convert To JSON
			    JSONObject jsonObj = new JSONObject(responseString);
			    Integer user_id = jsonObj.getInt("Id");
			    Integer Permission_Level = jsonObj.getInt("Permission_Level");
			    
			    responseInteger[0] = user_id;
			    responseInteger[1] = Permission_Level;
			    
			    Log.i(Contents.LOGIN_URL, String.valueOf(responseInteger)); // should return true or false
			} catch (ClientProtocolException e) {
				responseInteger[0] = -1;
			} catch (IOException e) {
				responseInteger[0] = -1;
			} catch (JSONException e) {
				responseInteger[0] = -1;
				e.printStackTrace();
			}
			
			if( responseInteger[1] > 0){
				DBUser.createUser(context, email, password, Integer.valueOf(responseInteger[0]), Integer.valueOf(responseInteger[1]));
			}
		}
		return responseInteger;
	}
	
    @Override
    protected void onPostExecute(Integer[] result) {
        super.onPostExecute(result);
        if( result[0] > 0){
        	Activity activityContext = ((Activity) context);
        	SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
			SharedPreferences.Editor editor = app_preferences.edit();
			editor.putInt("user_id", Integer.valueOf(result[0]));
			editor.putInt("Permission_Level", Integer.valueOf(result[1]));
			editor.commit();
			Intent i = new Intent(context, ScanInputActivity.class);
    		context.startActivity(i);
    		activityContext.finish();
        } else if(result[0] == 0) {
        	AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
        	alertbox.setMessage("Cannot find email: " + email);
        	alertbox.create();
        	alertbox.show();
        } else if(result[0] == -1){
        	AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
        	alertbox.setMessage("User not found locally! Please connect to the internet!");
        	alertbox.create();
        	alertbox.show();
        }
        mDialog.dismiss();
        Log.i("a", String.valueOf(result));
        
    }

}
