package com.planauts.vehiclescanner.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class InternetReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		boolean isConnected = activeNetInfo != null && activeNetInfo.isConnectedOrConnecting();
		
		if(isConnected){
			Toast.makeText(context, "Updating...Vehicle Scanner", Toast.LENGTH_SHORT).show();
			SyncHelper a = new SyncHelper(context);
			a.syncDatabase(null);
			Toast.makeText(context, "Internet Connection Detected Updating Vehicle Scanner", Toast.LENGTH_SHORT).show();
		}
		
	}

}
