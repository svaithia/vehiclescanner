 package com.planauts.vehiclescanner.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

public class SyncHelper{
	private Context context;

	public SyncHelper(Context context){
		this.context = context;
	}

	public void syncDatabase(TextView textview){
		SyncService syncService = new SyncService(context, textview);
		syncService.execute();
		Log.i("SYNC", "PUSHED");
	}
	
	public void resetDatabase(TextView textview){
        ResetService resetService = new ResetService(context, textview);
        resetService.execute();
		Log.i(this.getClass().getName(), "RESET");
	}
	
	public static String makeRequest(String url, List<NameValuePair> nameValuePairs){
		// Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(url);
	    
	    String responseString = "";
	    try {
	        // Add your data
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        HttpEntity entity = response.getEntity();
	        InputStream is = entity.getContent();
	        responseString = convertStreamToString(is);
	        Log.i(url, responseString);
	        
	    } catch (ClientProtocolException e) {
	    	e.printStackTrace();
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	    	responseString = "{ \"success\" : \"false\", \"msg\" : \"Internet Connection Not Found!\", \"date\" : \"2014-03-28 11:38:20\" }";
	    }
	    return responseString;
	}
	
	public static String convertStreamToString(InputStream is) {

	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();

	    String line = null;
	    try {
	        while ((line = reader.readLine()) != null) {
	        	if(line.length() == 0){
	        		sb.append("\n");
	        	}
	        	sb.append(line);
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb.toString();
	}
	
}
