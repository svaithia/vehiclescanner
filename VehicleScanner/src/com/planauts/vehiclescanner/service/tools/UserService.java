package com.planauts.vehiclescanner.service.tools;

import android.content.Context;
import android.telephony.TelephonyManager;

public class UserService {
	public UserService(){ }
	
	public static String getSerial(Context c){
		TelephonyManager mTelephonyMgr;
		 mTelephonyMgr = (TelephonyManager)c.getSystemService(Context.TELEPHONY_SERVICE);
		 String serial = mTelephonyMgr.getDeviceId();
		return serial;
	}
	
}
