package com.planauts.vehiclescanner.activity;

import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.planauts.vehiclescanner.R;
import com.planauts.vehiclescanner.R.array;
import com.planauts.vehiclescanner.R.id;
import com.planauts.vehiclescanner.R.layout;
import com.planauts.vehiclescanner.database.DBVehicle;
import com.planauts.vehiclescanner.database.DBVehicleInspection;
import com.planauts.vehiclescanner.model.VehicleInspection;

public class VehicleInspectionActivity extends Activity implements OnClickListener {
	RadioGroup rgStatus;
	EditText etNotes;
	VehicleInspection vi;
	boolean newVehicleInspection;
	
	@Override
	 public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.vehicle_inspection_form);
	  
	  rgStatus = (RadioGroup) findViewById(R.id.rgStatus);
	  etNotes = (EditText) findViewById(R.id.etNotes);
	  Button bSubmit = (Button) findViewById(R.id.bSubmit);
	  Button bHelp = (Button) findViewById(R.id.bHelp);
	  bSubmit.setOnClickListener(this);
	  bHelp.setOnClickListener(this);
	  
	  makeRadioButtons();
	  
	  String barcode = getIntent().getStringExtra("barcode");
	  vi = new VehicleInspection();
	  vi.setVehicle_barcode(Integer.parseInt(barcode));
	}
	
	private void makeRadioButtons(){
		String[] rg_status = getResources().getStringArray(R.array.rg_status);
		for(int i=0; i<rg_status.length; i++){
			RadioButton temp = new RadioButton(this);
			temp.setText(rg_status[i]);
			rgStatus.addView(temp);
		}
	}
	
    @Override
	protected void onResume(){
		super.onResume();
		SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
		int user_id = app_preferences.getInt("user_id", 0);		
		if(user_id == 0){
			finish();
		}
	}
	
	@Override
	public void onClick(View v){
		switch(v.getId()){
			case R.id.bSubmit:
				int status = rgStatus.indexOfChild(findViewById(rgStatus.getCheckedRadioButtonId()));
				
				if(status == -1){
					 Builder builder = new Builder(this);
		 	         builder.setCancelable(true);
		 	         builder.setTitle("Form submission error!");
		 	         builder.setMessage("Please make sure that a status is selected!");
		 	         builder.setInverseBackgroundForced(true);
		 	         builder.setPositiveButton("Okay :(",
		 	                  new DialogInterface.OnClickListener() {
		 	                      @Override
		 	                      public void onClick(DialogInterface dialog, int which) {
		 	                          dialog.dismiss();
		 	                      }
		 	                  });
		 	          AlertDialog alert = builder.create();
		 			  alert.show();  
		 			break;
				}
				
				SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
				int user_id = app_preferences.getInt("user_id", 0);
				String now = (String) DateFormat.format("yyyy-MM-dd hh:mm:ss", new Date());
				vi.setDate_modified(now);
				vi.setNotes(etNotes.getText().toString());
				vi.setStatus(status);
				vi.setDate_synced(null);
				vi.setUser_Id(user_id);
				
				DBVehicleInspection.createTempVehicleInspection(getApplicationContext(), vi);
				Toast.makeText(this, "New Vehicle Inspection added!", Toast.LENGTH_SHORT).show();
				
				Intent intent = new Intent(this, ScanInputActivity.class);
				finish();
				startActivity(intent);
				break;
			case R.id.bHelp:
				this.helpPopMenu();
				break;
			default:
				break;
		}
	}
	
	private void helpPopMenu(){
		String helpDocMsg = DBVehicle.getVehicleHelpNotes(getApplicationContext(), vi.getVehicle_barcode());
    	AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
    	alertbox.setMessage(helpDocMsg);
    	alertbox.create();
    	alertbox.show();
	}
}
