package com.planauts.vehiclescanner.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.planauts.vehiclescanner.R;
import com.planauts.vehiclescanner.R.id;
import com.planauts.vehiclescanner.R.layout;
import com.planauts.vehiclescanner.service.LoginService;

public class Login extends Activity implements OnClickListener {
	private EditText etEmail;
	private EditText etPassword;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		etEmail = (EditText) findViewById(R.id.etEmail);
		etPassword = (EditText) findViewById(R.id.etPassword);
		Button bLogin = (Button) findViewById(R.id.bLogin);
		bLogin.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.bLogin:
				LoginService loginService = new LoginService(this);
				loginService.execute(etEmail.getText().toString(), etPassword.getText().toString());
				break;
		}
	}

}
