package com.planauts.vehiclescanner.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.planauts.vehiclescanner.R;
import com.planauts.vehiclescanner.database.DBBuilding;
import com.planauts.vehiclescanner.database.DBFloor;
import com.planauts.vehiclescanner.database.DBMake;
import com.planauts.vehiclescanner.database.DBModel;
import com.planauts.vehiclescanner.database.DBNextServiceType;
import com.planauts.vehiclescanner.database.DBTerritory;
import com.planauts.vehiclescanner.database.DBVehicle;
import com.planauts.vehiclescanner.model.Building;
import com.planauts.vehiclescanner.model.Floor;
import com.planauts.vehiclescanner.model.Make;
import com.planauts.vehiclescanner.model.Model;
import com.planauts.vehiclescanner.model.NextServiceType;
import com.planauts.vehiclescanner.model.Territory;
import com.planauts.vehiclescanner.model.Vehicle;
import com.planauts.vehiclescanner.resources.SpinAdapter;

public class VehicleActivityRegular extends Activity implements OnClickListener{
	EditText etBarcode;
	Spinner sMake;
	Spinner sModel;
	Spinner sNext_Service_Type;
	
	Spinner sTerritory;
	Spinner sBuilding;
	Spinner sFloor;
	
	DatePicker dpService;
	
	Vehicle vehicle;
	EditText etDescription;
	EditText etHelpText;
	EditText etYear;
	boolean newVehicle;
	
	@Override
	 public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.vehicle_form_regular);
	  
	  etBarcode = (EditText)findViewById(R.id.etBarcode);
	  sModel = (Spinner) findViewById(R.id.sModel);
	  sMake = (Spinner) findViewById(R.id.sMake);
	  sNext_Service_Type = (Spinner) findViewById(R.id.sNext_Service_Type);
	  sTerritory = (Spinner) findViewById(R.id.sTerritory);
	  sBuilding = (Spinner) findViewById(R.id.sBuilding);
	  sFloor = (Spinner) findViewById(R.id.sFloor);
	  etDescription = (EditText)findViewById(R.id.etDescription);
	  dpService = (DatePicker)findViewById(R.id.dpService);
	  etYear = (EditText)findViewById(R.id.etYear);
	  etHelpText = (EditText)findViewById(R.id.etHelpText);
	  
	  Button bNext = (Button)findViewById(R.id.bNext);
	  bNext.setOnClickListener(this);
	  
	  
	  String barcode = getIntent().getStringExtra("barcode");
	  etBarcode.setText(barcode);
	  
	  setUpForm(Long.valueOf(barcode).longValue());
	}

    @Override
	protected void onResume() {
		super.onResume();
		SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
		int Permission_Level = app_preferences.getInt("Permission_Level", 0);
		
		switch(Permission_Level){
			case 0:
				finish();
				break;
			case 1:
			  	Intent intent = new Intent(this, VehicleActivityLite.class);
				intent.putExtra("barcode", getIntent().getStringExtra("barcode"));
				startActivity(intent);
				break;
		}
	}

	
	@Override
	 public void onClick(View v) {
		 switch (v.getId()) {
		 	case R.id.bNext:
		 		Model selectedModel = (Model) sModel.getSelectedItem();
		 		Floor selectedFloor = (Floor) sFloor.getSelectedItem();
		 		NextServiceType selectedNextServiceType = (NextServiceType) sNext_Service_Type.getSelectedItem();
		 		
		 		// check required fields are filled
		 		String descriptionText = etDescription.getText().toString();
		 		if(selectedModel.getID() == 0 || selectedFloor.getID() == 0 || TextUtils.isEmpty(descriptionText)){
		 			 Builder builder = new Builder(this);
		 	         builder.setCancelable(true);
		 	         builder.setTitle("Form submission error!");
		 	         builder.setMessage("Please make sure that the Description, Model and Floor information is filled!");
		 	         builder.setInverseBackgroundForced(true);
		 	         builder.setPositiveButton("Okay :(",
		 	                  new DialogInterface.OnClickListener() {
		 	                      @Override
		 	                      public void onClick(DialogInterface dialog, int which) {
		 	                          dialog.dismiss();
		 	                      }
		 	                  });
		 	          AlertDialog alert = builder.create();
		 			  alert.show();  
		 			break;
		 		}
		 		int chosenYear = TextUtils.isEmpty(etYear.getText().toString()) ? 0 :Integer.valueOf(etYear.getText().toString());
		 		
		 		String now = (String) DateFormat.format("yyyy-MM-dd hh:mm:ss", new Date());
				vehicle.setDescription(descriptionText);
				vehicle.setYear(chosenYear);
				vehicle.setModel_FK(selectedModel == null ? 0 : selectedModel.getID());
				vehicle.setFloor_FK(selectedFloor == null ? 0 : selectedFloor.getID());
				vehicle.setNext_Service_Type_FK(selectedNextServiceType == null ? 0 : selectedNextServiceType.getID());
				
				vehicle.setDate_modified(now);
				vehicle.setNext_Service_Date(dpService.getYear(), dpService.getMonth(), dpService.getDayOfMonth());
				vehicle.setHelp_check_list(etHelpText.getText().toString());
				
				boolean updated = false;
				if(!newVehicle){
					Vehicle oldVehicle = DBVehicle.getVehicle(getApplicationContext(), vehicle.getBarcode());
					updated = !(oldVehicle.getDescription().equals(vehicle.getDescription())) 
							|| !(oldVehicle.getModel_FK() == vehicle.getModel_FK()) 
							|| !(oldVehicle.getFloor_FK() == vehicle.getFloor_FK()) 
							|| !(oldVehicle.getNext_Service_Date() == vehicle.getNext_Service_Date()) 
							|| !(oldVehicle.getNext_Service_Type_FK() == vehicle.getNext_Service_Type_FK());
				} else {
					DBVehicle.createVehicle(getApplicationContext(), vehicle);

					SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(VehicleActivityRegular.this);
					SharedPreferences.Editor editor = app_preferences.edit();
					editor.putInt("last_model", vehicle.getModel_FK());
					editor.putInt("last_floor", vehicle.getFloor_FK());
					editor.commit();
					
			 		Toast.makeText(VehicleActivityRegular.this, "New Vehicle has been created!", Toast.LENGTH_SHORT).show();
				}

				if(updated){
					vehicle.setDate_synced(null);
					DBVehicle.updateVehicle(getApplicationContext(), vehicle);
			 		Toast.makeText(VehicleActivityRegular.this, "Updated Vehicle", Toast.LENGTH_SHORT).show(); 
				} else {
					Toast.makeText(VehicleActivityRegular.this, "No Vehicle changes made", Toast.LENGTH_SHORT).show();
				}

			  	Intent intent = new Intent(this, VehicleInspectionActivity.class);
				intent.putExtra("barcode", getIntent().getStringExtra("barcode"));
				startActivity(intent);
				
		 		break;
		 }
	 }
	
	private void setUpForm(long vid){
	  vehicle = DBVehicle.getVehicle(getApplicationContext(), vid);
	  newVehicle = (vehicle.getDate_modified() == null);
	  
	  // MODEL AND MAKE SPINNER
	  final List<Make> makeList = new ArrayList<Make>();
	  final List<Model> modelList = new ArrayList<Model>();
	  final List<NextServiceType> serviceTypeList = new ArrayList<NextServiceType>();

	  final Handler makeModelHandler = new Handler();
	  makeModelHandler.post(new Runnable() {
		@Override
		public void run() {
			  List<Make> tempMakeList = DBMake.getAllMakesWithDefault(VehicleActivityRegular.this);
			  makeList.addAll(tempMakeList);
			  SpinAdapter<Make> sMakeAdapter = new SpinAdapter<Make>(VehicleActivityRegular.this, R.layout.spinner_item, makeList);
			  final SpinAdapter<Model> sModelAdapter = new SpinAdapter<Model>(VehicleActivityRegular.this, R.layout.spinner_item, modelList);
			  final SpinAdapter<NextServiceType> sServiceTypeAdapter = new SpinAdapter<NextServiceType>(VehicleActivityRegular.this, R.layout.spinner_item, serviceTypeList);
			  
			  sMake.setAdapter(sMakeAdapter);
			  sModel.setAdapter(sModelAdapter);
			  sNext_Service_Type.setAdapter(sServiceTypeAdapter);
			  
			  sMake.setOnItemSelectedListener(new OnItemSelectedListener() {
			      @Override
			      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
			          sModel.setSelection(0, true);
			    	  Make make = makeList.get(position);
			          final List<Model> tempModelList = DBModel.getAllModelWithDefault(VehicleActivityRegular.this, make.getID());
			    	  
			          modelList.clear();
			          modelList.addAll(tempModelList);
			          sModelAdapter.notifyDataSetChanged();
			      }
			      @Override
			      public void onNothingSelected(AdapterView<?> adapter) {  }
			  });
			  
			  sModel.setOnItemSelectedListener(new OnItemSelectedListener() {
			      @Override
			      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
			    	  sNext_Service_Type.setSelection(0, true);
			    	  Model model= modelList.get(position);
			          final List<NextServiceType> tempServiceTypeList = DBNextServiceType.getAllNextServiceTypesWithDefault(VehicleActivityRegular.this, model.getID());
			    	  
			          serviceTypeList.clear();
			          serviceTypeList.addAll(tempServiceTypeList);
			          sServiceTypeAdapter.notifyDataSetChanged();
			      }
			      @Override
			      public void onNothingSelected(AdapterView<?> adapter) {  }
			  });
		}
	  });
	  
	  // TERRITORY -- BUILDING -- FLOOR -- SPINNER
	  final List<Territory> territoryList = new ArrayList<Territory>();
	  final List<Building> buildingList = new ArrayList<Building>();
	  final List<Floor> floorList = new ArrayList<Floor>();

	  final Handler territoryBuildingFloorHandler = new Handler();
	  territoryBuildingFloorHandler.post(new Runnable() {
		@Override
		public void run() {
			  List<Territory> tempTerritoryList = DBTerritory.getAllTerritoriesWithDefault(VehicleActivityRegular.this);
			  territoryList.addAll(tempTerritoryList);
			  SpinAdapter<Territory> sTerritoryAdapter = new SpinAdapter<Territory>(VehicleActivityRegular.this,  R.layout.spinner_item,  territoryList);
			  final SpinAdapter<Building> sBuildingAdapter = new SpinAdapter<Building>(VehicleActivityRegular.this,  R.layout.spinner_item,  buildingList);
			  final SpinAdapter<Floor> sFloorAdapter = new SpinAdapter<Floor>(VehicleActivityRegular.this,  R.layout.spinner_item,  floorList);
			  
			  sTerritory.setAdapter(sTerritoryAdapter);
			  sBuilding.setAdapter(sBuildingAdapter);
			  sFloor.setAdapter(sFloorAdapter);
			  
			  
			  sTerritory.setOnItemSelectedListener(new OnItemSelectedListener() {
			      @Override
			      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
			    	  sBuilding.setSelection(0, true);
			    	  sFloor.setSelection(0, true);
			    	  Territory territory = territoryList.get(position);
			    	  final List<Building> tempBuildingList = DBBuilding.getAllBuildingsWithDefault(VehicleActivityRegular.this, territory.getID());
			    	  
			    	  buildingList.clear();
			    	  buildingList.addAll(tempBuildingList);
			    	  sBuildingAdapter.notifyDataSetChanged();
			    	  
			    	  // initalize to default first value for floor
			    	  floorList.clear(); 
			    	  int buildingIndex = sBuilding.getSelectedItemPosition();
			    	  if(buildingIndex >= 0 && buildingList.size() > 0){
				    	  Building building = buildingList.size() > buildingIndex ? buildingList.get(buildingIndex) : buildingList.get(0);
				    	  final List<Floor> tempFloorList = DBFloor.getAllFloorsWithDefault(VehicleActivityRegular.this, building.getID());
				    	  floorList.addAll(tempFloorList);
			    	  }
			    	  sFloorAdapter.notifyDataSetChanged();
			      }
			      @Override
			      public void onNothingSelected(AdapterView<?> adapter) {  }
			  });
			  
			  sBuilding.setOnItemSelectedListener(new OnItemSelectedListener() {
			      @Override
			      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
			    	  sFloor.setSelection(0, true);
			    	  Building building = buildingList.get(position);
			    	  final List<Floor> tempFloorList = DBFloor.getAllFloorsWithDefault(VehicleActivityRegular.this, building.getID());
			    	  
			    	  floorList.clear();
			    	  floorList.addAll(tempFloorList);
			    	  sFloorAdapter.notifyDataSetChanged();
			      }
			      @Override
			      public void onNothingSelected(AdapterView<?> adapter) {  }
			  });
			  
		}
	  });
	  
	  if(newVehicle){
		  Toast.makeText(this, "New Vehicle", Toast.LENGTH_SHORT).show();
		  SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
		  
		  final int last_model = app_preferences.getInt("last_model", -1);
		  final Handler makeModelSpinner = new Handler();
			makeModelSpinner.post(new Runnable() {
			    @Override
			    public void run() {
			    	Model oldModel = DBModel.getModel(VehicleActivityRegular.this, last_model);
			    	if(oldModel != null){
					   	Make oldMake = DBMake.getMake(VehicleActivityRegular.this, oldModel.getMake_FK());
				
					   	int makeIndexSpinner = makeList.indexOf(oldMake);
					   	sMake.setSelection(makeIndexSpinner);
				    	
						List<Model> tempModelArray = DBModel.getAllModelWithDefault(VehicleActivityRegular.this, oldMake.getID());
						modelList.addAll(tempModelArray);
						final int modelIndexSpinner = tempModelArray.indexOf(oldModel);
						
						makeModelSpinner.postDelayed(new Runnable() {
						    @Override
						    public void run() {
						    	sModel.setSelection(modelIndexSpinner);
						    }
						}, 1000);
			    	}
			    }
			});
			
			final int last_floor = app_preferences.getInt("last_floor", -1);
			final Handler territoryBuildingFloorSpinner = new Handler();
		    territoryBuildingFloorSpinner.post(new Runnable() {
			    @Override
			    public void run() {
			    	final Floor oldFloor = DBFloor.getFloor(VehicleActivityRegular.this, last_floor);
			    	
			    	if(oldFloor != null){
				    	final Building oldBuilding = DBBuilding.getBuilding(VehicleActivityRegular.this, oldFloor.getBuilding_FK());
				    	
				    	if(oldBuilding != null){
					    	Territory oldTerritory = oldBuilding == null ? null : DBTerritory.getTerritory(VehicleActivityRegular.this, oldBuilding.getTerritory_FK());
		
					    	int territoryIndexSpinner = territoryList.indexOf(oldTerritory);
					    	sTerritory.setSelection(territoryIndexSpinner);
					    	
					    	List<Building> tempBuildingArray = DBBuilding.getAllBuildingsWithDefault(VehicleActivityRegular.this, oldTerritory.getID());
					    	final int buildingIndexSpinner = tempBuildingArray.indexOf(oldBuilding);
					    	buildingList.addAll(tempBuildingArray);
					    	
							makeModelSpinner.postDelayed(new Runnable() {
							    @Override
							    public void run() {
							    	sBuilding.setSelection(buildingIndexSpinner);
							    	
							    	List<Floor> tempFloorArray = DBFloor.getAllFloorsWithDefault(VehicleActivityRegular.this, oldBuilding.getID());
							    	final int floorIndexSpinner = tempFloorArray.indexOf(oldFloor);
							    	floorList.addAll(tempFloorArray);
							    	
									makeModelSpinner.postDelayed(new Runnable() {
									    @Override
									    public void run() {
									    	sFloor.setSelection(floorIndexSpinner);
									    }
									}, 1000);
							    }
							}, 1000);
							
				    	}
						
			    	}
			    	
			    }
		    });
		  
	  } else {
		  	final Handler makeModelSpinner = new Handler();
			makeModelSpinner.post(new Runnable() {
			    @Override
			    public void run() {
			    	final NextServiceType oldNextServiceType = DBNextServiceType.getNextServiceType(VehicleActivityRegular.this, vehicle.getNext_Service_Type_FK());
			    	final Model oldModel = DBModel.getModel(VehicleActivityRegular.this, vehicle.getModel_FK());
				   	Make oldMake = oldModel == null ? null : DBMake.getMake(VehicleActivityRegular.this, oldModel.getMake_FK());
			
				   	int makeIndexSpinner = makeList.indexOf(oldMake);
				   	sMake.setSelection(makeIndexSpinner);
			    	
					List<Model> tempModelArray = DBModel.getAllModelWithDefault(VehicleActivityRegular.this, oldMake.getID());
					modelList.addAll(tempModelArray);
					final int modelIndexSpinner = tempModelArray.indexOf(oldModel);
					List<NextServiceType> tempNextServiceTypeArray = DBNextServiceType.getAllNextServiceTypesWithDefault(VehicleActivityRegular.this, oldModel.getID());
					final int nextServiceTypeIndexSpinner = tempNextServiceTypeArray.indexOf(oldNextServiceType);
					
					makeModelSpinner.postDelayed(new Runnable() {
					    @Override
					    public void run() {
					    	sModel.setSelection(modelIndexSpinner);
//					    	serviceTypeList.addAll(tempNextServiceTypeArray);

					    	makeModelSpinner.postDelayed(new Runnable() {
							    @Override
							    public void run() {
							    	Log.i("A", nextServiceTypeIndexSpinner + "");
							    	sNext_Service_Type.setSelection(nextServiceTypeIndexSpinner);
							    }
							}, 1000);
					    	
					    }
					}, 1000);
			    }
			});
		    
		    final Handler territoryBuildingFloorSpinner = new Handler();
		    territoryBuildingFloorSpinner.post(new Runnable() {
			    @Override
			    public void run() {
			    	final Floor oldFloor = DBFloor.getFloor(VehicleActivityRegular.this, vehicle.getFloor_FK());
			    	final Building oldBuilding = oldFloor == null ? null : DBBuilding.getBuilding(VehicleActivityRegular.this, oldFloor.getBuilding_FK());
			    	Territory oldTerritory = oldBuilding == null ? null : DBTerritory.getTerritory(VehicleActivityRegular.this, oldBuilding.getTerritory_FK());

			    	int territoryIndexSpinner = territoryList.indexOf(oldTerritory);
			    	sTerritory.setSelection(territoryIndexSpinner);
			    	
			    	List<Building> tempBuildingArray = DBBuilding.getAllBuildingsWithDefault(VehicleActivityRegular.this, oldTerritory.getID());
			    	final int buildingIndexSpinner = tempBuildingArray.indexOf(oldBuilding);
			    	buildingList.addAll(tempBuildingArray);
			    	
					makeModelSpinner.postDelayed(new Runnable() {
					    @Override
					    public void run() {
					    	sBuilding.setSelection(buildingIndexSpinner);
					    	
					    	List<Floor> tempFloorArray = DBFloor.getAllFloorsWithDefault(VehicleActivityRegular.this, oldBuilding.getID());
					    	final int floorIndexSpinner = tempFloorArray.indexOf(oldFloor);
					    	floorList.addAll(tempFloorArray);
					    	
							makeModelSpinner.postDelayed(new Runnable() {
							    @Override
							    public void run() {
							    	sFloor.setSelection(floorIndexSpinner);
							    }
							}, 1000);
					    }
					}, 1000);
			    	
			    }
		    });
		    
		    Calendar nextServiceDate = vehicle.getNext_Service_Date();
		    dpService.updateDate(nextServiceDate.get(Calendar.YEAR), nextServiceDate.get(Calendar.MONTH), nextServiceDate.get(Calendar.DAY_OF_MONTH));
	    	etDescription.setText(vehicle.getDescription());
		    if(vehicle.getYear() != 0){
		    	etYear.setText(String.valueOf(vehicle.getYear()));
		    }
		    etDescription.setText(vehicle.getDescription());
	    	etHelpText.setText(vehicle.getHelp_check_list());
	    	Toast.makeText(VehicleActivityRegular.this, "Existing Vehicle Found!", Toast.LENGTH_SHORT).show();
	  	}
	}
	
}
