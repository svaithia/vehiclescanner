package com.planauts.vehiclescanner.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
//import android.provider.Contacts.Intents;


import android.widget.ToggleButton;

import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;
import com.planauts.vehiclescanner.R;

public class ScanInputActivity extends Activity implements OnClickListener {
	private static final int ZBAR_SCANNER_REQUEST = 0;
	private ToggleButton tbFlash;
	
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.activity_main);
	  
	  tbFlash = (ToggleButton) findViewById(R.id.tbFlash);
	  
	  Button bScan = (Button) findViewById(R.id.bScan);
	  bScan.setOnClickListener(this);
	  Button bNotScan = (Button) findViewById(R.id.bNotScan);
	  bNotScan.setOnClickListener(this);
	  Button bSettings= (Button) findViewById(R.id.bSettings);
	  bSettings.setOnClickListener(this);
	 }
	 
	 public void onClick(View v) {
	  Intent intent = null;
	  switch (v.getId()) {
	  	case R.id.bScan:
		  	   intent = new Intent(this, ZBarScannerActivity.class);
		  	   intent.putExtra("flash", tbFlash.isChecked());
			   startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
	  		break;
	  	case R.id.bNotScan:
	  		EditText qrInput = (EditText)findViewById(R.id.qrInput);
	  		String qrInputText = qrInput.getText().toString();
	  		
	  		if(!qrInputText.equals("")){
	  			SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
	  			int Permission_Level = app_preferences.getInt("Permission_Level", 0);
	  			
	  			switch(Permission_Level){
	  				case 1:
	  					intent = new Intent(this, VehicleActivityLite.class);
	  					break;
	  				case 2:
	  					intent = new Intent(this, VehicleActivityRegular.class);
	  					break;
	  			}
	  			
	  			intent.putExtra("barcode", qrInputText);
			  		startActivity(intent);
	  			
	  			
	  		} else {
	  			Toast.makeText(this, "Please enter a valid barcode", Toast.LENGTH_SHORT).show();
	  		}
	  		break;
	  	case R.id.bSettings:
	  		// Toast.makeText(this, "HI", Toast.LENGTH_SHORT).show();
	  		intent = new Intent(this, Settings.class);
	  		startActivity(intent);
	  		break;
	  	default:
	  		Toast.makeText(ScanInputActivity.this, "Error Button Not Found!", Toast.LENGTH_SHORT).show();
	  		break;
	  }
	 }
	 
    @Override
	protected void onResume() {
		super.onResume();
		SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
		int Permission_Level = app_preferences.getInt("Permission_Level", 0);
		if(Permission_Level == 0){
			finish();
		}
	}

	// Result
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{    
	    if (resultCode == RESULT_OK) 
	    {
	        String scanResult = data.getStringExtra(ZBarConstants.SCAN_RESULT);
	        Toast.makeText(this, "Scan Result = " + scanResult, Toast.LENGTH_SHORT).show();
	        Intent intent = new Intent(ScanInputActivity.this, VehicleActivityLite.class);
	        intent.putExtra("barcode", scanResult);
	        startActivity(intent);
	        
	    } else if(resultCode == RESULT_CANCELED) {
	        Toast.makeText(this, "Camera unavailable", Toast.LENGTH_SHORT).show();
	    }
	}
}
