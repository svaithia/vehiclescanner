package com.planauts.vehiclescanner.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.planauts.vehiclescanner.R;
import com.planauts.vehiclescanner.R.id;
import com.planauts.vehiclescanner.R.layout;
import com.planauts.vehiclescanner.service.SyncHelper;
import com.planauts.vehiclescanner.service.tools.UserService;

public class Settings extends Activity implements OnClickListener {
	SharedPreferences app_preferences;
	TextView tvSyncDate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);

		Button bResetDatabase = (Button) findViewById(R.id.bResetDatabase);
		bResetDatabase.setOnClickListener(this);
		Button bSyncNow = (Button) findViewById(R.id.bSyncNow);
		bSyncNow.setOnClickListener(this);
		Button bLogout = (Button) findViewById(R.id.bLogout);
		bLogout.setOnClickListener(this);

		app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
		String last_synced = app_preferences.getString("last_synced", "Unknown/Never");

		TextView tvUserId = (TextView) findViewById(R.id.tvUserId);
		tvUserId.setText(UserService.getSerial(this));
		
		tvSyncDate = (TextView) findViewById(R.id.tvSyncDate);
		tvSyncDate.setText(last_synced);
	}
	
    @Override
	protected void onResume() {
		super.onResume();
		SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
		int Permission_Level = app_preferences.getInt("Permission_Level", 0);		
		if(Permission_Level == 0){
			finish();
		}
	}
	
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.bSyncNow:
				SyncHelper a = new SyncHelper(this);
				a.syncDatabase(tvSyncDate);
				break;
			case R.id.bResetDatabase:
				SyncHelper b = new SyncHelper(this);
				b.resetDatabase(tvSyncDate);
				break;
			case R.id.bLogout:
	        	SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
				SharedPreferences.Editor editor = app_preferences.edit();
				editor.putInt("Permission_Level", 0);
				editor.commit();
				
				finish();
				
				break;
			default:
				Toast.makeText(this, "Button not found!", Toast.LENGTH_SHORT).show();
				break;
		}
	}
}
