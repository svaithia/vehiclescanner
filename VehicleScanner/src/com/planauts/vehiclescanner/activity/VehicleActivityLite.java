package com.planauts.vehiclescanner.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.planauts.vehiclescanner.R;
import com.planauts.vehiclescanner.database.DBBuilding;
import com.planauts.vehiclescanner.database.DBFloor;
import com.planauts.vehiclescanner.database.DBMake;
import com.planauts.vehiclescanner.database.DBModel;
import com.planauts.vehiclescanner.database.DBTerritory;
import com.planauts.vehiclescanner.database.DBVehicle;
import com.planauts.vehiclescanner.database.DatabaseHelper;
import com.planauts.vehiclescanner.model.Building;
import com.planauts.vehiclescanner.model.Floor;
import com.planauts.vehiclescanner.model.Make;
import com.planauts.vehiclescanner.model.Model;
import com.planauts.vehiclescanner.model.Territory;
import com.planauts.vehiclescanner.model.Vehicle;

public class VehicleActivityLite extends Activity implements OnClickListener{
	TextView tvBarcodeValue;
	TextView tvNameValue;
	TextView tvLocationValue;
	TextView tvServiceValue;
	TextView tvDescriptionValue;

	Vehicle vehicle;
	
	@Override
	 public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.vehicle_form_lite);

	  tvBarcodeValue = (TextView)findViewById(R.id.tvBarcodeValue);
	  tvNameValue = (TextView)findViewById(R.id.tvNameValue);
	  tvLocationValue = (TextView)findViewById(R.id.tvLocationValue);
	  tvServiceValue = (TextView)findViewById(R.id.tvServiceValue);
	  tvDescriptionValue = (TextView)findViewById(R.id.tvDescriptionValue);
	  
	  Button bNext = (Button)findViewById(R.id.bNext);
	  bNext.setOnClickListener(this);
	  
	  String barcode = getIntent().getStringExtra("barcode");
	  setUpForm(Long.valueOf(barcode).longValue());
	}
	
    @Override
	protected void onResume() {
		super.onResume();
		SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
		int Permission_Level = app_preferences.getInt("Permission_Level", 0);
		switch(Permission_Level){
			case 0:
				finish();
				break;
			case 2:
			  	Intent intent = new Intent(this, VehicleActivityRegular.class);
				intent.putExtra("barcode", getIntent().getStringExtra("barcode"));
				startActivity(intent);
				break;
		}
	}

	
	@Override
	 public void onClick(View v) {
		 switch (v.getId()) {
		 	case R.id.bNext:
			  	Intent intent = new Intent(this, VehicleInspectionActivity.class);
				intent.putExtra("barcode", getIntent().getStringExtra("barcode"));
				startActivity(intent);
		 		break;
		 }
	 }
	
	private void setUpForm(long vid){
	  vehicle = DBVehicle.getVehicle(getApplicationContext(), vid);
	  boolean newVehicle = vehicle.getDate_modified() == null;
	  
	  if(newVehicle){
		  Builder builder = new Builder(this);
          builder.setCancelable(true);
          builder.setTitle("Error!");
          builder.setMessage("You don't have enough permission to create your own vehicle. Look up an existing one instead");
          builder.setInverseBackgroundForced(true);
          builder.setPositiveButton("Okay :(",
                  new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                          finish();
                      }
                  });
          AlertDialog alert = builder.create();
		  alert.show();  
	  } else {
		  
		  final Handler handler = new Handler();
		    handler.post(new Runnable() {
			    @Override
			    public void run() {
					StringBuilder nameValue = new StringBuilder();
					StringBuilder locationValue = new StringBuilder();
			    	
			    	Model oldModel = DBModel.getModel(VehicleActivityLite.this, vehicle.getModel_FK());
				   	Make oldMake = oldModel == null ? null : DBMake.getMake(VehicleActivityLite.this, oldModel.getMake_FK());
			    	
			    	Floor oldFloor = DBFloor.getFloor(VehicleActivityLite.this, vehicle.getFloor_FK());
			    	Building oldBuilding = oldFloor == null ? null : DBBuilding.getBuilding(VehicleActivityLite.this, oldFloor.getBuilding_FK());
			    	Territory oldTerritory = oldBuilding == null ? null : DBTerritory.getTerritory(VehicleActivityLite.this, oldBuilding.getTerritory_FK());
			    	
			    	if(oldMake == null || oldTerritory == null){
			    		return;
			    	}
			    	
			    	nameValue.append(String.valueOf(vehicle.getYear()));
			    	nameValue.append(" "); 
			    	nameValue.append(oldMake.getName());
			    	nameValue.append(" - "); 
			    	nameValue.append(oldModel.getName());
			    	
			    	locationValue.append(oldTerritory.getName());
			    	locationValue.append(" : ");
			    	locationValue.append(oldBuilding.getName());
			    	locationValue.append(" : ");
			    	locationValue.append(oldFloor.getName());
			    	
			    	tvBarcodeValue.setText(String.valueOf(vehicle.getBarcode()));
					tvNameValue.setText(nameValue.toString());
					tvLocationValue.setText(locationValue);
					tvServiceValue.setText(DatabaseHelper.DATE_FORMAT.format(vehicle.getNext_Service_Date().getTime()));
					tvDescriptionValue.setText(vehicle.getDescription());
			    }
		    });
		  
		 
		  Toast.makeText(this, "Existing Vehicle Found!", Toast.LENGTH_SHORT).show();
	  }
	}
	
}
