package com.planauts.vehiclescanner.model;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Building {
	private int ID;
	private String Date_Modified;
	private String Date_Created;
	private String Name;
	private int Territory_FK;
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getDate_Modified() {
		return Date_Modified;
	}
	public void setDate_Modified(String date_Modified) {
		Date_Modified = date_Modified;
	}
	public String getDate_Created() {
		return Date_Created;
	}
	public void setDate_Created(String date_Created) {
		Date_Created = date_Created;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}

	@Deprecated
	public Building(){}
	
	public Building(int id, String dm, String ds, String n, int tfk){
		setID(id);
		setDate_Modified(dm);
		setDate_Created(ds);
		setName(n);
		setTerritory_FK(tfk);
	}
	
	public JSONObject writeJSON(){
		JSONObject result = new JSONObject();
		try {
			result.put("ID", getID());
			result.put("Name", getName());
			result.put("Date_Modified", getDate_Modified());
			result.put("Date_Synced", getDate_Created());
			result.put("Territory_FK", getTerritory_FK());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public void readJSON(JSONObject jsonObject){
		try {
			this.setID(jsonObject.getInt("ID"));
			this.setName(jsonObject.getString("Name"));
			this.setDate_Modified(jsonObject.getString("Date_Modified"));
			this.setDate_Created(jsonObject.getString("Date_Created"));
			this.setTerritory_FK(jsonObject.getInt("Territory_FK"));
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}
	
	public static String writeJSON(List<Building> buildings){
		JSONArray jsonArray = new JSONArray();
		for(Building building: buildings){
			jsonArray.put(building.writeJSON());
		}
		return jsonArray.toString();
	}
	public int getTerritory_FK() {
		return Territory_FK;
	}
	public void setTerritory_FK(int territory_FK) {
		Territory_FK = territory_FK;
	}
	
	@Override
	public String toString(){
		return getName();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof Building))return false;
	    Building otherFloor = (Building)other;
	    
	    return this.getID() == otherFloor.getID() &&
	    		this.getDate_Created().equals(otherFloor.getDate_Created()) &&
	    		this.getDate_Modified().equals(otherFloor.getDate_Modified()) &&
	    		this.getName().equals(otherFloor.getName()) &&
	    		this.getTerritory_FK() == otherFloor.getTerritory_FK();
	}
	
	  @Override
      public int hashCode() {
         final int prime = 31;
         int result = 1;
         result = prime * result + getID();
         result = prime * result + getDate_Created().hashCode();
         result = prime * result + getDate_Modified().hashCode();
         result = prime * result + getName().hashCode();
         result = prime * result + getTerritory_FK();
         result = prime * result + getID();
         return result;
      }
	

}
