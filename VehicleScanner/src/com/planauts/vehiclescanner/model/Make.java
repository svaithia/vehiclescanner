package com.planauts.vehiclescanner.model;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Make {
	private int ID;
	private String Date_Modified;
	private String Date_Created;
	private String Name;
	private int Sort;
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getDate_Modified() {
		return Date_Modified;
	}
	public void setDate_Modified(String date_Modified) {
		Date_Modified = date_Modified;
	}
	public String getDate_Created() {
		return Date_Created;
	}
	public void setDate_Created(String date_created) {
		Date_Created = date_created;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	
	public Make(){}
	
	public Make(int id, String dm, String dc, String n, int sort){
		setID(id);
		setDate_Modified(dm);
		setDate_Created(dc);
		setName(n);
		setSort(sort);
	}
	
	public JSONObject writeJSON(){
		JSONObject result = new JSONObject();
		try {
			result.put("ID", getID());
			result.put("Name", getName());
			result.put("Date_Modified", getDate_Modified());
			result.put("Date_Created", getDate_Created());
			result.put("Sort", getSort());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public void readJSON(JSONObject jsonObject){
		try {
			this.setID(jsonObject.getInt("ID"));
			this.setName(jsonObject.getString("Name"));
			this.setDate_Modified(jsonObject.getString("Date_Modified"));
			this.setDate_Created(jsonObject.getString("Date_Created"));
			this.setSort(jsonObject.getInt("Sort"));
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}
	
	public static String writeJSON(List<Make> makes){
		JSONArray jsonArray = new JSONArray();
		for(Make make: makes){
			jsonArray.put(make.writeJSON());
		}
		return jsonArray.toString();
	}
	
	@Override
	public String toString(){
		return getName();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof Make))return false;
	    Make otherMake = (Make)other;
	    
	    return this.getID() == otherMake.getID() &&
	    		this.getDate_Created().equals(otherMake.getDate_Created()) &&
	    		this.getDate_Modified().equals(otherMake.getDate_Modified()) &&
	    		this.getName().equals(otherMake.getName()) &&
	    		this.getSort() == otherMake.getSort();
	}
	
	  @Override
      public int hashCode() {
         final int prime = 31;
         int result = 1;
         result = prime * result + getID();
         result = prime * result + getDate_Created().hashCode();
         result = prime * result + getDate_Modified().hashCode();
         result = prime * result + getName().hashCode();
         result = prime * result + getID();
         result = prime * result + getSort();
         return result;
      }
	public int getSort() {
		return Sort;
	}
	public void setSort(int sort) {
		Sort = sort;
	}

	

}
