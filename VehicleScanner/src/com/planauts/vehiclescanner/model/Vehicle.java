package com.planauts.vehiclescanner.model;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.planauts.vehiclescanner.database.DatabaseHelper;

public class Vehicle {
	private int barcode;
//	private int name; // Model foreign key
	private int Model_FK;
	private int Floor_FK;
	private int Next_Service_Type_FK;
	private String description;
	private String date_modified;
	private String date_synced;
	private String help_check_list;
	private Calendar Next_Service_Date;
	private int Year;
	
	public Vehicle(){}
	
	@Deprecated
	public Vehicle(int Barcode, int mfk, String Description, int ffk, String nsd, int year){
		setBarcode(Barcode);
		setModel_FK(mfk);
		setFloor_FK(ffk);
		setDescription(Description);
		setNext_Service_Date(nsd);
		setYear(year);
	}
	
	@Deprecated
	public Vehicle(int Barcode, int mfk, String Description, String date_modified, int ffk, String nsd, int year){
		setBarcode(Barcode);
		setModel_FK(mfk);
		setFloor_FK(ffk);
		setDescription(Description);
		setDate_modified(date_modified);
		setNext_Service_Date(nsd);
		setYear(year);
	}
	
	public Vehicle(int Barcode, String Description, int mfk, int year, int ffk,
			String nsd, String help_check_list, String date_modified, String Date_synced, int nstfk){
		setBarcode(Barcode);
		setDescription(Description);
		setModel_FK(mfk);
		setYear(year);
		setFloor_FK(ffk);
		setNext_Service_Date(nsd);
		setHelp_check_list(help_check_list);
		setDate_modified(date_modified);
		setDate_synced(Date_synced);
		setNext_Service_Type_FK(nstfk);
	}
	
	public int getBarcode() {
		return barcode;
	}

	public void setBarcode(int barcode) {
		this.barcode = barcode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate_synced() {
		return date_synced;
	}

	public void setDate_synced(String date_synced) {
		this.date_synced = date_synced;
	}

	public String getDate_modified() {
		return date_modified;
	}
	

	public void setDate_modified(String date_modified) {
		this.date_modified = date_modified;
	}
	
	public JSONObject writeJSON() {
	  JSONObject result = new JSONObject();
	  try {
		  result.put("Barcode", getBarcode());
		  result.put("Model_FK", getModel_FK());
		  result.put("Floor_FK", getFloor_FK());
		  result.put("Description", getDescription());
		  result.put("Date_Modified", getDate_modified());
		  result.put("Date_Synced", getDate_synced());
		  result.put("Next_Service_Date", DatabaseHelper.DATE_FORMAT.format(getNext_Service_Date().getTime()));
		  result.put("Next_Service_Type_FK", getNext_Service_Type_FK());
		  result.put("Help_Check_List", getHelp_check_list());
		  result.put("Year", getYear());
	  } catch (JSONException e) {
		  e.printStackTrace();
	  }
	  return result;
	}
	
	public void readJSON(JSONObject jsonObject){
		try {
			this.setBarcode(jsonObject.getInt("Barcode"));
			this.setDescription(jsonObject.getString("Description"));
			this.setDate_modified(jsonObject.getString("Date_Modified"));
			this.setDate_synced(jsonObject.getString("Date_Synced"));
			this.setModel_FK(jsonObject.getInt("Model_FK"));
			this.setFloor_FK(jsonObject.getInt("Floor_FK"));
			this.setHelp_check_list(jsonObject.getString("Help_Check_List"));
			this.setNext_Service_Date(jsonObject.getString("Next_Service_Date"));
			this.setNext_Service_Type_FK(jsonObject.getInt("Next_Service_Type_FK"));
			this.setYear(jsonObject.getInt("Year"));
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}
	
	public static String writeJSON(List<Vehicle> vehicles){
		JSONArray jsonArray = new JSONArray();
		for(Vehicle v: vehicles){
			jsonArray.put(v.writeJSON());
		}
		return jsonArray.toString();
	}

	public String getHelp_check_list() {
		return help_check_list;
	}

	public void setHelp_check_list(String help_check_list) {
		this.help_check_list = help_check_list;
	}

	public int getModel_FK() {
		return Model_FK;
	}

	public void setModel_FK(int model_FK) {
		Model_FK = model_FK;
	}

	public int getFloor_FK() {
		return Floor_FK;
	}

	public void setFloor_FK(int floor_FK) {
		Floor_FK = floor_FK;
	}

	public Calendar getNext_Service_Date() {
		Log.i(this.getClass().getName(), Next_Service_Date.getTime().toString());
		return Next_Service_Date;
	}

	public void setNext_Service_Date(String next_Service_Date) {
		Calendar calendar = Calendar.getInstance();
		try {
			Date date = DatabaseHelper.DATE_FORMAT.parse(next_Service_Date);
			calendar.setTime(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Next_Service_Date = calendar;
	}
	
	public void setNext_Service_Date(int year, int month, int date){
		if(Next_Service_Date == null){
			Next_Service_Date = Calendar.getInstance();
		}
		Next_Service_Date.set(year, month, date);
	}

	public int getYear() {
		return Year;
	}

	public void setYear(int year) {
		this.Year = year;
	}

	public int getNext_Service_Type_FK() {
		return Next_Service_Type_FK;
	}

	public void setNext_Service_Type_FK(int next_Service_Type_FK) {
		Next_Service_Type_FK = next_Service_Type_FK;
	}
}
