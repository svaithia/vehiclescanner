package com.planauts.vehiclescanner.model;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Model {
	private int ID;
	private String Date_Modified;
	private String Date_Created;
	private String Name;
	private int Make_FK;
	private int Sort;
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getDate_Modified() {
		return Date_Modified;
	}
	public void setDate_Modified(String date_Modified) {
		Date_Modified = date_Modified;
	}

	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	
	public int getMake_FK() {
		return Make_FK;
	}
	public void setMake_FK(int make_FK) {
		Make_FK = make_FK;
	}
	
	public Model(){}
	
	public Model(int id, String dm, String ds, String n, int make_fk, int sort){
		setID(id);
		setDate_Modified(dm);
		setDate_Created(ds);
		setName(n);
		setMake_FK(make_fk);
		setSort(sort);
	}
	
	public JSONObject writeJSON(){
		JSONObject result = new JSONObject();
		try {
			result.put("ID", getID());
			result.put("Name", getName());
			result.put("Date_Modified", getDate_Modified());
			result.put("Date_Synced", getDate_Created());
			result.put("Make_FK", getMake_FK());
			result.put("Sort", getSort());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public void readJSON(JSONObject jsonObject){
		try {
			this.setID(jsonObject.getInt("ID"));
			this.setName(jsonObject.getString("Name"));
			this.setDate_Modified(jsonObject.getString("Date_Modified"));
			this.setDate_Created(jsonObject.getString("Date_Created"));
			this.setMake_FK(jsonObject.getInt("Make_FK"));
			this.setSort(jsonObject.getInt("Sort"));
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}
	
	public static String writeJSON(List<Model> models){
		JSONArray jsonArray = new JSONArray();
		for(Model model: models){
			jsonArray.put(model.writeJSON());
		}
		return jsonArray.toString();
	}
	
	public String getDate_Created() {
		return Date_Created;
	}
	
	public void setDate_Created(String date_Created) {
		Date_Created = date_Created;
	}

	@Override
	public String toString(){
		return getName();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof Model))return false;
	    Model otherModel = (Model)other;
	    
	    return this.getID() == otherModel.getID() &&
	    		this.getDate_Created().equals(otherModel.getDate_Created()) &&
	    		this.getDate_Modified().equals(otherModel.getDate_Modified()) &&
	    		this.getName().equals(otherModel.getName()) &&
	    		this.getMake_FK() == otherModel.getMake_FK() &&
	    		this.getSort() == otherModel.getSort();
	}
	
	  @Override
      public int hashCode() {
         final int prime = 31;
         int result = 1;
         result = prime * result + getID();
         result = prime * result + getDate_Created().hashCode();
         result = prime * result + getDate_Modified().hashCode();
         result = prime * result + getName().hashCode();
         result = prime * result + getMake_FK();
         result = prime * result + getID();
         result = prime * result + getSort();
         return result;
      }
	public int getSort() {
		return Sort;
	}
	public void setSort(int sort) {
		Sort = sort;
	}



}
