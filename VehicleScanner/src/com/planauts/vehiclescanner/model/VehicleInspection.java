package com.planauts.vehiclescanner.model;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class VehicleInspection {
	private int id;
	private String date_synced;
	private String date_modified;
	private int vehicle_barcode;
	private String notes;
	private int status;
	private int user_id;
	
	public VehicleInspection(){}
	
	public VehicleInspection(int Id, String Date_synced, int Vehicle_barcode, String Notes, int Status){
		setId(Id);
		setDate_synced(Date_synced);
		setVehicle_barcode(Vehicle_barcode);
		setNotes(Notes);
		setStatus(Status);
	}
	
	public VehicleInspection(String Date_synced, int Vehicle_barcode, String Notes, int Status){
		setVehicle_barcode(Vehicle_barcode);
		setNotes(Notes);
		setStatus(Status);
	}
	
	public VehicleInspection(int Vehicle_barcode, String Notes, int Status){
		setVehicle_barcode(Vehicle_barcode);
		setNotes(Notes);
		setStatus(Status);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDate_synced() {
		return date_synced;
	}
	public void setDate_synced(String date_synced) {
		this.date_synced = date_synced;
	}
	public int getVehicle_barcode() {
		return vehicle_barcode;
	}
	public void setVehicle_barcode(int vehicle_barcode) {
		this.vehicle_barcode = vehicle_barcode;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public String getDate_modified() {
		return date_modified;
	}

	public int getUser_Id() {
		return user_id;
	}

	public void setUser_Id(int id) {
		this.user_id = id;
	}
	
	public void setDate_modified(String date_modified) {
		this.date_modified = date_modified;
	}

	public JSONObject writeJSON() {
		JSONObject result = new JSONObject();
		try {
			result.put("Id", getId());
			result.put("Date_Synced", getDate_synced());
			result.put("Date_Modified", getDate_modified());
			result.put("Vehicle_Barcode", getVehicle_barcode());
			result.put("Notes", getNotes());
			result.put("Status", getStatus());
			result.put("User_Id", getUser_Id());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	  	return result;
	}
	
	public void readJSON(JSONObject jsonObject){
		try {
			this.setId(jsonObject.getInt("Id"));
			this.setNotes(jsonObject.getString("Notes"));
			this.setDate_modified(jsonObject.getString("Date_Modified"));
			this.setDate_synced(jsonObject.getString("Date_Synced"));
			this.setStatus(jsonObject.getInt("Status"));
			this.setVehicle_barcode(jsonObject.getInt("Vehicle_Barcode"));
			this.setUser_Id(jsonObject.getInt("User_Id"));
	    
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}
	
	public static String writeJSON(List<VehicleInspection> vi){
		JSONArray jsonArray = new JSONArray();
		for(VehicleInspection v: vi){
			jsonArray.put(v.writeJSON());
		}
		return jsonArray.toString();
	}
}
