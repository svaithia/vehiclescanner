package com.planauts.vehiclescanner.model;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Territory {
	private int ID;
	private String Date_Modified;
	private String Date_Created;
	private String Name;
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getDate_Modified() {
		return Date_Modified;
	}
	public void setDate_Modified(String date_Modified) {
		Date_Modified = date_Modified;
	}

	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	
	public Territory(){}
	
	public Territory(int id, String dm, String dc, String n){
		setID(id);
		setDate_Modified(dm);
		setDate_Created(dc);
		setName(n);
	}
	
	public JSONObject writeJSON(){
		JSONObject result = new JSONObject();
		try {
			result.put("ID", getID());
			result.put("Name", getName());
			result.put("Date_Modified", getDate_Modified());
			result.put("Date_Synced", getDate_Created());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public void readJSON(JSONObject jsonObject){
		try {
			this.setID(jsonObject.getInt("ID"));
			this.setName(jsonObject.getString("Name"));
			this.setDate_Modified(jsonObject.getString("Date_Modified"));
			this.setDate_Created(jsonObject.getString("Date_Created"));
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}
	
	public static String writeJSON(List<Territory> territories){
		JSONArray jsonArray = new JSONArray();
		for(Territory territory: territories){
			jsonArray.put(territory.writeJSON());
		}
		return jsonArray.toString();
	}
	public String getDate_Created() {
		return Date_Created;
	}
	public void setDate_Created(String date_Created) {
		Date_Created = date_Created;
	}
	
	@Override
	public String toString(){
		return getName();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof Territory))return false;
	    Territory otherMake = (Territory)other;
	    
	    return this.getID() == otherMake.getID() &&
	    		this.getDate_Created().equals(otherMake.getDate_Created()) &&
	    		this.getDate_Modified().equals(otherMake.getDate_Modified()) &&
	    		this.getName().equals(otherMake.getName());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getID();
		result = prime * result + getDate_Created().hashCode();
		result = prime * result + getDate_Modified().hashCode();
		result = prime * result + getName().hashCode();
		result = prime * result + getID();
		return result;
	}

}
